
function countdown(seconds) {
	return new Promise(function(resolve, reject) {
		for(let i=seconds; i>=0; i--) {
			setTimeout(function() {
				//if (i===13) return reject(new Error('BAD LUCK!'));
			   //.then will not be executed if reject happens
				if (i>0) console.log(i + '...');
					else resolve(new function() { console.log('GO!'); });
			}, (seconds-i)*1000);
		}
	});
}

countdown(13).then(function() {
	console.log('Finished!');
},
	function(err) {
		console.log('Error caught: '+ err.message);
	}
).then(function() { console.log('End of .then chain') });

