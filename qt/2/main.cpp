#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject *rect = engine.rootObjects().first()->findChild<QObject*>("rect");
    QVariant returnedValue;
    QVariant n = 15;
    QMetaObject::invokeMethod(rect, "get_fibo",
            Q_RETURN_ARG(QVariant, returnedValue),
            Q_ARG(QVariant, n));

    qDebug() << "get_fibo() returned:" << returnedValue.toString();

    return app.exec();
}

