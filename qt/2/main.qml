import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    id: window
    visible: true
    width: 640
    height: 480

    Rectangle {
        id: rect
        objectName: 'rect' //will look for it from C++
        width: parent.width-100; height: parent.height-100
        border.color: 'red'
        border.width: 10        
        anchors.centerIn: parent

        function get_fibo(n) {
            function fibo(n) {
                if (n < 1)
                    return 0;
                if (n <= 2)
                    return 1;
               return fibo(n-1) + fibo(n-2);
            }
            var ret = '';
            for (var i=0; i<n; i++)
                ret += fibo(i) + ' ';
            return ret;
        }

        Text {
            text: rect.get_fibo(15)
            anchors.centerIn: rect
        }
    }
}
