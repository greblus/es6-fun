#include "backend.h"

BackEnd::BackEnd(QObject *parent) :
    QObject(parent)
{
    m_Text = QString("Default text");
}

QString BackEnd::getText()
{
    return m_Text;
}

void BackEnd::setText(const QString &text)
{
    if (text == m_Text)
        return;

    m_Text = text;
    emit textChanged();
}
