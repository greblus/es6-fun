import QtQuick 2.11
import QtQuick.Window 2.11
import net.greblus.backend 1.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0

Window {
    id: mainwindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    BackEnd {
        id: backend
        caption: "Qt is so cool!"
        onTextChanged: setcaption()

        function setcaption() {
            desc.text = backend.getText().split("").reverse().join("");
        }
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        Text {
            id: desc
            text: backend.caption
            font.pointSize: 11
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            fontSizeMode: Text.HorizontalFit
        }

        Button {
            id: button
            text: qsTr("Button")
            font.pointSize: 12
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: false
            onClicked: backend.setText(textInput.text)
        }

        TextInput {
            id: textInput
            text: qsTr("Enter description here and click on the button")
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            selectByMouse: true

            font.pixelSize: 16
            BorderImage {
                source: "b.png"
                border.left: 3; border.top: 3
                border.right: 3; border.bottom: 3
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.text.length*parent.font.pointSize
                height: parent.font.pixelSize+10
                anchors.centerIn: parent
            }
        }
    }
}
