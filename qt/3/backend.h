#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QString>

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString caption READ getText WRITE setText NOTIFY textChanged)

public:
    explicit BackEnd(QObject *parent = nullptr);

    Q_INVOKABLE QString getText();
    Q_INVOKABLE void setText(const QString &text);

private:
    QString m_Text;

signals:
    void textChanged();

};

#endif // BACKEND_H


