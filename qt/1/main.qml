import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("JS fun 1")

    Rectangle {
        id: rect
        width: parent.width-100; height: parent.height-100
        border.color: 'red'
        border.width: 10
        color: 'yellow'
        anchors.centerIn: parent

        function mouseAreaClicked(area) {
            console.log("Clicked in area at: " + area.x + ", " + area.y);
        }

        Text {
            text: "Mousearea - click me"
            anchors.centerIn: parent
        }

        MouseArea {
            id: area; x: 0; y: 0
            height: rect.height; width: rect.width
            onClicked: parent.mouseAreaClicked(area)
        }
    }
}
